package main

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"time"
	pb "bitbucket.org/perfectgentlemande/go-grpc-example/example"
)

const defaultName = "man"
const address = "localhost:3000"

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.SayHello(ctx, &pb.HelloRequest{Name: defaultName})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Println(r.MessageOfTheDay)
	log.Printf("Current time is: %s", time.Unix(r.CurrentDate.Seconds, int64(r.CurrentDate.Nanos)).String())

}
