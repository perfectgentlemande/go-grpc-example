package main

import (
	"github.com/golang/protobuf/ptypes/timestamp"
	"google.golang.org/grpc"
	"log"
	"context"
	pb "bitbucket.org/perfectgentlemande/go-grpc-example/example"
	"net"
	"time"
)

type server struct {
	pb.UnimplementedGreeterServer
}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Received name: %v", in.GetName())

	now := time.Now()
	return &pb.HelloReply{
		MessageOfTheDay: "Hello " + in.GetName() + "!",
		CurrentDate: &timestamp.Timestamp{
			Seconds: now.Unix(),
			Nanos: int32(now.Nanosecond()),
		},
	}, nil
}

const port = ":3000"
func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
